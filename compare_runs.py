#!/usr/bin/env python3

import django
import argparse
import json
import sys

django.setup()

from CIResults.models import RunConfig  # noqa
from CIResults.serializers import RunConfigDiffSerializer  # noqa
from CIResults.email import Email  # noqa


def get_runconfig_or_die(name):
    try:
        return RunConfig.objects.get(name=name)
    except RunConfig.DoesNotExist:
        print("ERROR: The runconfig '{}' does not exist in the database".format(name))
        sys.exit(1)


# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("runcfg1", help="Name of the runconfig that will serve as the base of the comparison")
parser.add_argument("runcfg2", help="Name of the runconfig that will compared to runcfg1")

parser.add_argument("-s", "--summary", action="store_true",
                    help="Only print the summary of the change instead of the full json")
parser.add_argument("--no_compress", action="store_true",
                    help="Do not compress results in the summary")
parser.add_argument("-sf", "--summary_file", default=None,
                    help="Location of the file where to store the summary")
parser.add_argument("-r", "--result", action="store_true",
                    help="Only print the status of the difference (SUCCESS, WARNING or FAILURE)")
parser.add_argument("-rf", "--result_file", default=None,
                    help="Location of the file where to store the result")

parser.add_argument("--to", action='append', default=[],
                    help="Send an email containing the comparison to these addresses")
parser.add_argument("--subject", default="[CI Bug Log] {runcfg1} -> {runcfg2} comparison",
                    help="Subject to use for the email. NOTICE: You can use {runcfg1} and {runcfg2} "
                         "in the subject, they will get replaced by their full names")

args = parser.parse_args()

# Get the two runconfigs, then compare them
runcfg1 = get_runconfig_or_die(name=args.runcfg1)
runcfg2 = get_runconfig_or_die(name=args.runcfg2)
diff = runcfg1.compare(runcfg2, no_compress=args.no_compress)

# FIXME: Since we do not have an easy way to get rid of flip-floppers, let's
# consider warnings as a success for now!
status = diff.status
if status == "WARNING":
    status = "SUCCESS"

summary_file = open(args.summary_file, mode="w") if args.summary_file is not None else sys.stdout
result_file = open(args.result_file, mode="w") if args.result_file is not None else sys.stdout

if args.summary:
    print(diff.text, file=summary_file)
if args.result:
    print(status, file=result_file)

if not args.summary and not args.result:
    ser = RunConfigDiffSerializer()
    print(json.dumps(ser.to_representation(diff), sort_keys=True, indent=4))

if len(args.to) > 0:
    Email(args.subject.format(runcfg1=runcfg1, runcfg2=runcfg2), diff.text, args.to).send()

sys.exit(0)
