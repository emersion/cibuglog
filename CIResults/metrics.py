from django.db.models import Count, Sum, Max, Min, Avg
from django.utils import timezone
from django.utils.functional import cached_property

from .models import Issue, Bug, BugTracker, KnownFailure, Machine
from .models import IssueFilterAssociated, TestsuiteRun
from .models import TestResult, RunConfig, TextStatus, Test

from collections import namedtuple, OrderedDict, defaultdict
from dateutil.relativedelta import relativedelta, MO

from datetime import timedelta

from copy import deepcopy

import dateutil
import hashlib
import json


class Period:
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __repr__(self):
        return "{}->{}".format(repr(self.start), repr(self.end))

    def __str__(self):
        return "{}->{}".format(self.start, self.end)

    def __eq__(self, other):
        return self.start == other.start and self.end == other.end


class Periodizer:
    @classmethod
    def from_json(cls, json_string):
        params = json.loads(json_string)

        count = int(params.get('count', 30))

        end_date_str = params.get('end')
        if end_date_str is not None:
            end_date = timezone.make_aware(dateutil.parser.parse(end_date_str))
        else:
            end_date = timezone.now()

        period_str = params.get('period', 'week')
        if period_str == 'month':
            period_offset = relativedelta(months=1, day=1, hour=0, minute=0,
                                          second=0, microsecond=0)
            period = relativedelta(months=1)
            real_end = end_date + period_offset
            description = 'last {} months before {}'.format(count, real_end.strftime("%B %Y"))
        elif period_str == 'day':
            period_offset = relativedelta(days=1, hour=0, minute=0,
                                          second=0, microsecond=0)
            period = relativedelta(days=1)
            real_end = end_date + period_offset

            description = 'last {} days'.format(count)
            if end_date_str is not None:
                description += " before " + real_end.date().isoformat()
        else:  # fall-through: default to week
            period_offset = relativedelta(days=1, hour=0, minute=0,
                                          second=0, microsecond=0,
                                          weekday=MO(1))
            period = relativedelta(weeks=1)
            real_end = end_date + period_offset
            description = 'last {} weeks before {} ({})'.format(count, real_end.date().isoformat(),
                                                                real_end.strftime("WW-%Y.%W"))

        return cls(period_offset=period_offset, period=period, period_count=count,
                   end_date=end_date, description=description)

    def __init__(self, period_offset=relativedelta(days=1, hour=0, minute=0,
                                                   second=0, microsecond=0,
                                                   weekday=MO(1)),
                 period=relativedelta(weeks=1), period_count=30,
                 end_date=timezone.now(),
                 description="last 30 weeks"):
        self.period_offset = period_offset
        self.period = period
        self.period_count = period_count
        self.end_date = end_date
        self.description = description

        self.end_cur_period = end_date + period_offset

    def __iter__(self):
        # Reset the current position
        self.cur_period = self.period_count
        return self

    def __next__(self):
        if self.cur_period == 0:
            raise StopIteration

        self.cur_period -= 1
        cur_time = self.end_cur_period - self.cur_period * self.period
        return Period(cur_time - self.period, cur_time)


PeriodOpenItem = namedtuple("PeriodOpenItem", ('period', 'label', 'active', 'new', 'closed'))


class ItemCountTrend:
    def __init__(self, items, fields=[], periodizer=None):
        self.items = items
        self.periodizer = periodizer

        self.fields = defaultdict(list)
        for i in items:
            for field in fields:
                values = getattr(i, field)
                if not isinstance(values, str):
                    values = len(values)
                self.fields[field].append(values)

    @property
    def stats(self):
        r = {}
        if self.periodizer is not None:
            r["period_desc"] = self.periodizer.description
        for field, values in self.fields.items():
            r[field] = values
        return r


class OpenCloseCountTrend(ItemCountTrend):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, fields=['label', 'active', 'new', 'closed'], **kwargs)


def bugs_followed_since():
    bt = BugTracker.objects.exclude(components_followed_since=None).order_by('-components_followed_since').first()
    if bt is not None:
        return bt.components_followed_since
    else:
        return None


# TODO: Add tests for all these functions

def metrics_issues_over_time(label_format="WW-%Y-%W",
                             periodizer=None):
    if periodizer is None:
        periodizer = Periodizer(relativedelta(days=1, hour=0, minute=0,
                                              second=0, microsecond=0,
                                              weekday=MO(1)),
                                period=relativedelta(weeks=1),
                                period_count=30,
                                end_date=timezone.now())

    issues = list()
    for i, period in enumerate(periodizer):
        active = set(Issue.objects.filter(added_on__lt=period.end).exclude(archived_on__lt=period.end))
        if i == 0:
            existing = set(Issue.objects.filter(added_on__lt=period.start).exclude(archived_on__lt=period.start))
            new = active - existing
            closed = existing - active
        else:
            new = active - issues[-1].active
            closed = issues[-1].active - active
        issues.append(PeriodOpenItem(period.start, period.start.strftime(label_format),
                                     active, new, closed))

    return OpenCloseCountTrend(issues, periodizer=periodizer)


def metrics_bugs_over_time(label_format="WW-%Y-%W",
                           periodizer=None,
                           user_filters={}):
    if periodizer is None:
        periodizer = Periodizer(relativedelta(days=1, hour=0, minute=0,
                                              second=0, microsecond=0,
                                              weekday=MO(1)),
                                period=relativedelta(weeks=1),
                                period_count=30,
                                end_date=timezone.now())

    # Find out what is the earliest components_followed_since and make sure we don't go that far
    earliest_followed_since = bugs_followed_since()
    if earliest_followed_since is None:
        return OpenCloseCountTrend([], periodizer=periodizer)

    # Get a list of periods, from oldest to newest
    bug_periods = []
    for period in periodizer:
        # Ignore all the periods not fully covered
        if period.start < earliest_followed_since:
            continue
        bug_periods.append(PeriodOpenItem(period, period.start.strftime(label_format), [], [], []))

    filtered_bugs = Bug.from_user_filters(**user_filters).objects.exclude(closed__lt=earliest_followed_since)
    for bug in filtered_bugs.prefetch_related("tracker"):
        # ignore bugs that we are not following
        if bug.component not in bug.tracker.components_followed_list:
            continue

        for bp in bug_periods:
            if bug.created >= bp.period.start and bug.created < bp.period.end:
                bp.new.append(bug)
            elif bug.closed is not None and bug.closed >= bp.period.start and bug.closed < bp.period.end:
                bp.closed.append(bug)
            elif bug.created < bp.period.start and (bug.closed is None or bug.closed >= bp.period.end):
                bp.active.append(bug)

    return OpenCloseCountTrend(bug_periods, periodizer=periodizer)


class Bin:
    def __init__(self, upper_limit, label):
        self.items = set()
        self.upper_limit = upper_limit
        self.label = label


class TimeBinizer:
    def __init__(self, items, bins=[Bin(timedelta(hours=1), "under an hour"),
                                    Bin(timedelta(hours=6), "under 6 hours"),
                                    Bin(timedelta(days=1), "under a day"),
                                    Bin(timedelta(days=7), "under a week"),
                                    Bin(timedelta(days=30), "under a month"),
                                    Bin(timedelta(days=90), "under 3 months"),
                                    Bin(timedelta(days=365), "under a year"),
                                    Bin(timedelta.max, "over a year")]):
        self._bins = deepcopy(bins)
        for item, time in items:
            for bin in self._bins:
                if time < bin.upper_limit:
                    bin.items.add(item)
                    break

    @property
    def bins(self):
        return self._bins

    @property
    def stats(self):
        return {"items_count": [len(b.items) for b in self.bins],
                "label": [b.label for b in self.bins]}


def metrics_issues_ttr(date=timezone.now(), period=timedelta(days=30)):
    request = Issue.objects.filter(archived_on__lt=date, archived_on__gt=date-period).exclude(archived_on__isnull=True)
    return TimeBinizer([(item, item.archived_on - item.added_on) for item in request])


def metrics_open_issues_age(date=timezone.now()):
    request = Issue.objects.filter(archived_on=None)
    now = timezone.now()
    return TimeBinizer([(item, now - item.added_on) for item in request])


def metrics_failure_filing_delay(date=timezone.now(), period=timedelta(days=30)):
    request = KnownFailure.objects.filter(manually_associated_on__gt=date-period)
    request = request.filter(result__ts_run__runconfig__temporary=False)
    bins = [Bin(timedelta(hours=1), "under an hour"),
            Bin(timedelta(hours=8), "under 8 hours"),
            Bin(timedelta(days=1), "under a day"),
            Bin(timedelta(days=3), "under three days"),
            Bin(timedelta(days=7), "under a week"),
            Bin(timedelta(days=30), "under a month"),
            Bin(timedelta.max, "over a month")]
    return TimeBinizer([(item, item.filing_delay) for item in request], bins=bins)


def metrics_bugs_ttr(date=timezone.now(), period=timedelta(days=30), user_filters={}):
    request = Bug.from_user_filters(**user_filters).objects.filter(closed__lt=date, closed__gt=date-period)
    request = request.exclude(closed__isnull=True).prefetch_related("tracker")
    bugs = set(filter(lambda b: b.component in b.tracker.components_followed_list, request))
    return TimeBinizer([(item, item.closed - item.created) for item in bugs])


def metrics_open_bugs_age(date=timezone.now(), user_filters={}):
    request = Bug.from_user_filters(**user_filters).objects.filter(closed=None).prefetch_related("tracker")
    bugs = set(filter(lambda b: b.component in b.tracker.components_followed_list, request))
    now = timezone.now()
    return TimeBinizer([(item, now - item.created) for item in bugs])


class PieChartData:
    # results needs to be a dictionary mapping a label (string) to a number
    def __init__(self, results):
        self._results = OrderedDict()

        for label, value in sorted(results.items(), key=lambda kv: kv[1], reverse=True):
            self._results[label] = value

    def label_to_color(self, label):
        blake2 = hashlib.blake2b()
        blake2.update(label.encode())
        return "#" + blake2.hexdigest()[-7:-1]

    @property
    def colors(self):
        return [self.label_to_color(label) for label in self._results.keys()]

    def stats(self):
        return {'results': list(self._results.values()),
                'labels': list(self._results.keys()),
                'colors': list(self.colors)}


def metrics_testresult_statuses_stats(results):
    statuses = {}
    for result in results:
        label = str(result.status)
        statuses[label] = statuses.get(label, 0) + 1
    return PieChartData(statuses)


def metrics_knownfailure_statuses_stats(failures):
    return metrics_testresult_statuses_stats([f.result for f in failures])


def metrics_testresult_machines_stats(results):
    machines = {}
    for result in results:
        label = str(result.ts_run.machine)
        machines[label] = machines.get(label, 0) + 1
    return PieChartData(machines)


def metrics_knownfailure_machines_stats(failures):
    return metrics_testresult_machines_stats([f.result for f in failures])


def metrics_testresult_tests_stats(results):
    tests = {}
    for result in results:
        label = str(result.test)
        tests[label] = tests.get(label, 0) + 1
    return PieChartData(tests)


def metrics_knownfailure_tests_stats(failures):
    return metrics_testresult_tests_stats([f.result for f in failures])


def metrics_knownfailure_issues_stats(failures):
    # Since the query has likely already been made, we can't prefetch the
    # issues anymore... so let's hand roll it!
    matched_ifas = set()
    for failure in failures:
        matched_ifas.add(failure.matched_ifa_id)
    ifa_to_issues = dict()
    ifas = IssueFilterAssociated.objects.filter(id__in=matched_ifas)
    for e in ifas.prefetch_related('issue', 'issue__bugs', 'issue__bugs__tracker'):
        ifa_to_issues[e.id] = e.issue

    issues = {}
    for failure in failures:
        label = str(ifa_to_issues.get(failure.matched_ifa_id, None))
        issues[label] = issues.get(label, 0) + 1
    return PieChartData(issues)


def metrics_testresult_issues_stats(failures):
    total = []
    for failure in failures:
        total.extend(failure.known_failures.all())
    return metrics_knownfailure_issues_stats(total)


class Rate:
    def __init__(self, count, total):
        self.count = count
        self.total = total

    @property
    def percent(self):
        return self.count / self.total * 100.0

    def __repr__(self):
        return "Rate({}, {})".format(self.count, self.total)

    def __str__(self):
        return "{:.2f}% ({} / {})".format(self.percent, self.count, self.total)


class LineChartData:
    # results needs to be a dictionary mapping a label (string) to a number
    def __init__(self, results, x_labels, line_label_colors={}):
        self._results = OrderedDict()
        self.x_labels = x_labels
        self.line_label_colors = line_label_colors

        for label, value in results.items():
            self._results[label] = value

    def label_to_color(self, label):
        blake2 = hashlib.blake2b()
        blake2.update(label.encode())
        default = "#" + blake2.hexdigest()[-7:-1]

        return self.line_label_colors.get(label, default)

    def stats(self):
        dataset = []
        for line_label, result in self._results.items():
            color = self.label_to_color(line_label)
            entry = {'label': line_label,
                     'borderColor': color,
                     'backgroundColor': color,
                     'data': result}
            dataset.append(entry)

        return {'dataset': dataset, 'labels': self.x_labels}


class MetricPassRateHistory:
    filtering_model = TestResult

    def _queryset_to_dict(self, Model, ids, *prefetch_related):
        return dict((o.pk, o) for o in Model.objects.filter(id__in=ids).prefetch_related(*prefetch_related))

    def __init__(self, user_filters):
        self.user_filters = user_filters
        self._total_results = 0

        self.query = self.filtering_model.from_user_filters(**user_filters)

        # Fetch the user-filtered from the database
        filtered_results = self.query.objects
        filtered_results = filtered_results.prefetch_related('ts_run__runconfig__name',
                                                             'ts_run__machine__name',
                                                             'test__name', 'status__name')
        db_results = filtered_results.values('ts_run__runconfig__id', 'status__id').annotate(count=Count("status__id"))

        # Get the runconfigs and statuses
        runconfigs = self._queryset_to_dict(RunConfig, [r['ts_run__runconfig__id'] for r in db_results])
        statuses = self._queryset_to_dict(TextStatus, [r['status__id'] for r in db_results], 'testsuite')

        # Add the results to the history, then compute the totals
        runconfigs_tmp = defaultdict(lambda: defaultdict(int))
        statuses_tmp = set()
        for r in db_results:
            status = statuses[r['status__id']]
            runconfigs_tmp[runconfigs[r['ts_run__runconfig__id']]][status] = r['count']
            statuses_tmp.add(status)

        # Order the statuses and runconfigs
        runconfigs_ordered = sorted(runconfigs_tmp.keys(), key=lambda r: r.added_on, reverse=True)
        statuses_ordered = sorted(statuses_tmp, key=lambda r: str(r))

        # Create the final result structures
        self.runconfigs = OrderedDict()   # [runconfig][status] = Rate()
        self.statuses = OrderedDict()     # [status][runconfig] = Rate()
        for status in statuses_ordered:
            self.statuses[status] = OrderedDict()
        for runconfig in runconfigs_ordered:
            # Compute the total for the run
            total = 0
            for status in statuses_ordered:
                total += runconfigs_tmp[runconfig][status]

            # Add the passrate to both the runconfig-major and status-major table
            self.runconfigs[runconfig] = OrderedDict()
            for status in statuses_ordered:
                passrate = Rate(runconfigs_tmp[runconfig][status], total)
                self.runconfigs[runconfig][status] = self.statuses[status][runconfig] = passrate

            # Keep track of how many results we found (useful or the most hit issues)
            self._total_results += total

    @cached_property
    def chart(self):
        # The runconfigs are ordered from newest to oldest, reverse that
        runconfigs = list(reversed(self.runconfigs.keys()))

        chart_data = defaultdict(list)
        for runconfig in runconfigs:
            for status in self.runconfigs[runconfig]:
                passrate = self.runconfigs[runconfig][status]
                chart_data[status.name].append(format((passrate.count / passrate.total) * 100, '.3f'))

        status_colors = dict()
        for status in self.statuses:
            status_colors[status.name] = status.color

        return LineChartData(chart_data, [r.name for r in runconfigs], status_colors)

    @cached_property
    def most_hit_issues(self):
        # Make the smallest possible query to the DB, getting us the hit rate
        query = TestResult.from_user_filters(**self.user_filters).objects
        query = query.prefetch_related('ts_run__runconfig__name', 'ts_run__machine__name', 'test__name',
                                       'status__name').values('known_failure__matched_ifa__issue')
        db_results = query.annotate(count=Count("known_failure__matched_ifa__issue")).order_by('-count')

        # Make a dictionary going from the hit issues' id to issues
        issues = dict()
        issue_ids = [r['known_failure__matched_ifa__issue'] for r in db_results if r['count'] > 0]
        for issue in Issue.objects.filter(id__in=issue_ids).prefetch_related('bugs', 'bugs__tracker'):
            issues[issue.id] = issue

        # Now order them from the most hit to the least hit
        ordered_issues = OrderedDict()
        for r in db_results:
            if r['count'] > 0:
                issue = issues[r['known_failure__matched_ifa__issue']]
                ordered_issues[issue] = Rate(r['count'], self._total_results)

        return ordered_issues


class MetricRuntimeHistory:
    filtering_model = TestsuiteRun

    def _queryset_to_dict(self, Model, ids, *prefetch_related):
        return dict((o.pk, o) for o in Model.objects.filter(id__in=ids).prefetch_related(*prefetch_related))

    def __init__(self, user_filters, average_per_machine=False):
        self.user_filters = user_filters
        self.average_per_machine = average_per_machine

        self.query = self.filtering_model.from_user_filters(**user_filters)

        # Fetch the user-filtered from the database
        filtered_results = self.query.objects
        filtered_results = filtered_results.filter(duration__gt=timedelta(seconds=0))  # Ignore negative exec times
        db_results = filtered_results.values('id', 'runconfig_id',
                                             'machine_id').annotate(total=Sum("duration"))

        # Get the runconfigs and machines
        runconfigs = self._queryset_to_dict(RunConfig, [r['runconfig_id'] for r in db_results])
        machines = self._queryset_to_dict(Machine, [r['machine_id'] for r in db_results], "aliases")
        self._tsr_ids = [r['id'] for r in db_results]

        # Add the results to the history, then compute the totals
        self.participating_machines = defaultdict(lambda: defaultdict(set))
        runconfigs_tmp = defaultdict(lambda: defaultdict(timedelta))
        machines_tmp = set()
        for r in db_results:
            # de-duplicate shard machines by replacing a machine by its shard
            machine = machines[r['machine_id']]
            if machine.aliases is not None:
                machine = machine.aliases

            runconfigs_tmp[runconfigs[r['runconfig_id']]][machine] += r['total']
            self.participating_machines[runconfigs[r['runconfig_id']]][machine].add(r['machine_id'])
            machines_tmp.add(machine)

        # Order the machines and runconfigs
        runconfigs_ordered = sorted(runconfigs_tmp.keys(), key=lambda r: r.added_on, reverse=True)
        machines_ordered = sorted(machines_tmp, key=lambda m: str(m))

        # Create the final result structures
        self.runconfigs = OrderedDict()   # [runconfig][machine] = Rate()
        self.machines = OrderedDict()     # [machine][runconfig] = Rate()
        for machine in machines_ordered:
            self.machines[machine] = OrderedDict()
        for runconfig in runconfigs_ordered:
            # Add the runtime to both the runconfig-major and machine-major table
            self.runconfigs[runconfig] = OrderedDict()
            for machine in machines_ordered:
                total_time = runconfigs_tmp[runconfig][machine]
                machine_count = len(self.participating_machines[runconfig][machine])
                if average_per_machine:
                    if machine_count > 0:
                        actual_time = total_time / machine_count
                    else:
                        actual_time = timedelta()
                else:
                    actual_time = total_time
                self.runconfigs[runconfig][machine] = self.machines[machine][runconfig] = actual_time

    def _machine_to_name(self, machine):
        counts = list()
        for runconfig in self.runconfigs:
            counts.append(len(self.participating_machines[runconfig][machine]))

        c_min = min(counts)
        c_max = max(counts)

        if c_max == 1:
            return str(machine)
        elif c_min == c_max:
            return "{} ({})".format(str(machine), c_min)
        else:
            return "{} ({}-{})".format(str(machine), c_min, c_max)

    @cached_property
    def chart(self):
        # The runconfigs are ordered from newest to oldest, reverse that
        runconfigs = list(reversed(self.runconfigs.keys()))

        chart_data = defaultdict(list)
        for runconfig in runconfigs:
            for machine in self.runconfigs[runconfig]:
                time = self.runconfigs[runconfig][machine]
                if time != timedelta():
                    minutes = format(time.total_seconds() / 60, '.2f')
                else:
                    minutes = "null"
                chart_data[self._machine_to_name(machine)].append(minutes)

        machines_colors = dict()
        for machine in self.machines:
            machines_colors[self._machine_to_name(machine)] = machine.color

        return LineChartData(chart_data, [r.name for r in runconfigs], machines_colors)

    @cached_property
    def longest_tests(self):
        TestRunTime = namedtuple("TestRunTime", ('test', 'machine', 'min', 'avg', 'max'))
        longest_tests = []

        db_results = TestResult.objects.filter(ts_run_id__in=self._tsr_ids)
        db_results = db_results.values("test_id", "ts_run__machine_id")
        db_results = db_results.annotate(d_avg=Avg("duration"),
                                         d_min=Min("duration"),
                                         d_max=Max("duration")).order_by("-d_max")
        db_results = db_results[:1000]

        tests = self._queryset_to_dict(Test, [r['test_id'] for r in db_results], "testsuite")
        machines = self._queryset_to_dict(Machine, [r['ts_run__machine_id'] for r in db_results], "aliases")

        # Create all the TestRunTime objects
        aliased_machines = defaultdict(list)
        for r in db_results:
            test = tests[r['test_id']]
            machine = machines[r['ts_run__machine_id']]
            runtime = TestRunTime(test, machine, r['d_min'], r['d_avg'], r['d_max'])

            if machine.aliases is None:
                longest_tests.append(runtime)
            else:
                aliased_machines[(test, machine.aliases)].append(runtime)

        # Deduplicate all the data for aliased machines
        for key, results in aliased_machines.items():
            test, machine = key

            # Aggregate all the values (Avg won't be super accurate, but good-enough)
            d_min = results[0].min
            avg_sum = timedelta()
            d_max = results[0].max
            for result in results:
                d_min = min(d_min, result.min)
                d_max = max(d_max, result.max)
                avg_sum += result.avg

            longest_tests.append(TestRunTime(results[0].test, machine, d_min, avg_sum / len(results), d_max))

        return sorted(longest_tests, key=lambda r: r.max, reverse=True)[:100]
