from django import template
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.timesince import timesince


# Define some filters for django's
register = template.Library()


@register.filter
def howlongago(value):
    if value is None:
        return "Never"
    else:
        return "{} ago".format(timesince(value))


# The point of this filter is to convert a list of db objects into a coma-separated
# list of values which have been transformed using a template.
#
# Use this filter like this: {{mylist|csl:"obj:path/to/template"}} with obj being
# the name expected by the template
@register.filter
def csl(value, args=None):
    obj_name, template = args.split(':')
    return mark_safe(", ".join([render_to_string(template, {obj_name: obj}) for obj in value]))
