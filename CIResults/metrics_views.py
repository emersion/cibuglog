from django.contrib import messages
from django.db import transaction
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.utils import timezone
from django.views.decorators.http import require_http_methods

from collections import namedtuple, defaultdict

from .models import BugTracker, Bug, Test, Machine, RunConfig, Issue, UnknownFailure
from .models import BugTrackerAccount, BugComment

from .metrics import metrics_issues_over_time, metrics_bugs_over_time, metrics_issues_ttr, metrics_bugs_ttr
from .metrics import metrics_open_issues_age, metrics_open_bugs_age, metrics_failure_filing_delay
from .metrics import MetricPassRateHistory, MetricRuntimeHistory, Periodizer

from datetime import timedelta


def api_metrics(request):
    metrics = {
        "issues": Issue.objects.filter(archived_on=None).count(),
        "machine": Machine.objects.all().count(),
        "suppressed_tests": Test.objects.filter(vetted_on=None).exclude(first_runconfig=None).count(),
        "suppressed_machines": Machine.objects.filter(vetted_on=None).count(),
        "tests": Test.objects.all().count(),
        "unknown_failures": UnknownFailure.objects.filter(result__ts_run__runconfig__temporary=False).count(),
        "version": 1,
    }

    return JsonResponse(metrics)


def metrics_generic_history(request, History, history_kwargs, template_path, **kwargs):
    public = kwargs.get("public", False)

    # convert the user filters to a normal dictionary to prevent issues when
    # inserting new values
    requested_filters = dict(request.GET.copy())

    # Default to a sane query if no query has been made
    query = History.filtering_model.from_user_filters(**requested_filters)
    if query.is_valid and query.is_empty:
        runconfigs = RunConfig.objects.filter(temporary=False).order_by('-id')[:10]
        runconfigs_str = ','.join(["'{}'".format(r.name) for r in runconfigs])
        requested_filters['query'] = "runconfig_name IS IN [{}]".format(runconfigs_str)
    elif not query.is_valid:
        messages.error(request, "Filtering error: " + query.error)

    context = {
        "public": public,
        'history': History(requested_filters, **history_kwargs),
    }
    return render(request, template_path, context)


def metrics_passrate_history(request, **kwargs):
    return metrics_generic_history(request, MetricPassRateHistory, {}, 'CIResults/metrics_passrate.html', **kwargs)


def metrics_runtime_history(request, **kwargs):
    return metrics_generic_history(request, MetricRuntimeHistory,
                                   {"average_per_machine": request.GET.get('average', '0') == '1'},
                                   'CIResults/metrics_runtime.html', **kwargs)


def metrics_overview(request, **kwargs):
    public = kwargs.get("public", False)

    period = request.GET.get("period", None)
    periodizer = None if period is None else Periodizer.from_json(period)

    context = {
            "public": public,
            "issues_weekly":  metrics_issues_over_time(periodizer=periodizer).stats,
            "issues_ttr": metrics_issues_ttr().stats,
            "issues_age": metrics_open_issues_age().stats,
            "failure_filing_delay": metrics_failure_filing_delay().stats,

            "bugs_weekly":  metrics_bugs_over_time(periodizer=periodizer).stats,
            "bugs_ttr": metrics_bugs_ttr().stats,
            "bugs_age": metrics_open_bugs_age().stats,
        }
    return render(request, 'CIResults/metrics_overview.html', context)


def metrics_bugs(request, **kwargs):
    public = kwargs.get("public", False)

    period = request.GET.get("period", None)
    periodizer = None if period is None else Periodizer.from_json(period)

    requested_filters = request.GET.copy()
    query = Bug.from_user_filters(**requested_filters)
    if not query.is_valid:
        messages.error(request, "Filtering error: " + query.error)

    bugs_weekly = metrics_bugs_over_time(user_filters=requested_filters, periodizer=periodizer).stats
    bugs_ttr = metrics_bugs_ttr(user_filters=requested_filters).stats
    bugs_age = metrics_open_bugs_age(user_filters=requested_filters).stats

    context = {
            "public": public,

            "bugs_weekly":  bugs_weekly,
            "bugs_ttr": bugs_ttr,
            "bugs_age": bugs_age,

            'filters_model': Bug,
            'query': query,
        }
    return render(request, 'CIResults/metrics_bugs.html', context)


def open_bugs(request, **kwargs):
    public = kwargs.get("public", False)

    # Parse the user request
    user_query = Bug.from_user_filters(**request.GET.copy())
    if not user_query.is_valid:
        messages.error(request, "Filtering error: " + user_query.error)

    # If the user request is empty, just select everything
    selected_bugs = Bug.objects.all() if user_query.is_empty else user_query.objects
    selected_bugs = selected_bugs.prefetch_related('tracker', 'assignee__person', "creator__person", "issue_set")

    referenced_bugs = set([b.id for b in selected_bugs if len(b.issue_set.all()) > 0])
    all_followed_and_open_bugs = set([b for b in selected_bugs if b.is_open and (b.id in referenced_bugs or
                                                                                 b.component in b.tracker.components_followed_list)])  # noqa
    all_comments = BugComment.objects.filter(bug__in=selected_bugs).prefetch_related('bug',
                                                                                     'bug__tracker',
                                                                                     "account",
                                                                                     "account__person")

    # HACK: Preload all the comments and assign them to their respective bugs
    comments = defaultdict(list)
    for comment in all_comments:
        comments[(comment.bug.id, comment.bug.tracker.id)].append(comment)
    for bug in all_followed_and_open_bugs:
        bug.comments_cached = comments[(bug.id, bug.tracker.id)]

    # Now, classify the urgency
    overdue = list()
    close = list()
    other = list()
    for bug in sorted(all_followed_and_open_bugs, key=lambda b: b.effective_priority, reverse=True):
        time_left = bug.SLA_remaining_time
        if time_left < timedelta(seconds=0):
            overdue.append(bug)
        elif time_left < timedelta(days=7):
            close.append(bug)  # pragma: no cover (TODO: requires to control the current time)
        else:
            other.insert(-1, bug)

    # Create a store for involvement statistics
    DeveloperStatistics = namedtuple('DeveloperStatistics', ('bugs_created', 'bugs_involved',
                                                             'bugs_assigned', 'bugs_still_open',
                                                             'bugs_closed', 'all_comments'))
    devs = dict()
    for dev in sorted(BugTrackerAccount.objects.filter(is_developer=True), key=lambda a: str(a)):
        devs[dev] = DeveloperStatistics(bugs_created=set(), bugs_assigned=set(), bugs_involved=set(),
                                        bugs_still_open=set(), bugs_closed=set(), all_comments=set())

    # Get statistics on who created bugs
    days_ago_30 = timezone.now() - timedelta(days=30)
    for bug in selected_bugs:
        if bug.created is not None and bug.created > days_ago_30:
            if bug.creator is not None and bug.creator.is_developer:
                devs[bug.creator].bugs_created.add(bug)

    # Count how many bugs are assigned
    for bug in all_followed_and_open_bugs:
        if bug.assignee in devs:
            devs[bug.assignee].bugs_assigned.add(bug)

    # Go through all comments and assign them to their author
    for comment in all_comments:
        # Ignore comments older than 30 days and from non-dev accounts
        if comment.created_on <= days_ago_30 or not comment.account.is_developer:
            continue

        dev = devs[comment.account]
        dev.all_comments.add(comment)
        dev.bugs_involved.add(comment.bug)
        if comment.bug.is_open:
            dev.bugs_still_open.add(comment.bug)
        else:
            dev.bugs_closed.add(comment.bug)

    # Remove the developers who did not interact in the past 30 days
    for dev in list(devs.keys()):
        stats = devs[dev]
        if len(stats.bugs_created) == 0 and len(stats.bugs_assigned) == 0 and len(stats.all_comments) == 0:
            del devs[dev]

    context = {
        "trackers": BugTracker.objects.all(),
        "all_open_bugs": all_followed_and_open_bugs,
        "bugs_overdue": overdue,
        "bugs_close_to_deadline": close,
        "bugs_other": other,
        "developers": devs,
        "public": public,

        'filters_model': Bug,
        'query': user_query,
    }
    return render(request, 'CIResults/open_bugs.html', context)


@transaction.atomic
@require_http_methods(["POST"])
def bug_flag_for_update(request, pk):
    bug = get_object_or_404(Bug.objects.select_for_update(), pk=pk)

    if not bug.is_being_updated:
        bug.flagged_as_update_pending_on = timezone.now()
        bug.save()
        status = 200
    else:
        status = 409

    return JsonResponse(data={}, status=status)
